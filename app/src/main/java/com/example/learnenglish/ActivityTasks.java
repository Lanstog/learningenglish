package com.example.learnenglish;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class ActivityTasks extends AppCompatActivity {
    Question curQuestion;
    static boolean engMode;
    ArrayList<Word> tasks;
    int answers;
    int rightAnswers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);
        tasks = GetValue.getTasks();
        //answers = tasks.size() - 2;
        answers = 0;
        updateQuestion();
        final Button button1 = findViewById(R.id.firstAnswer);
        final Button button2 = findViewById(R.id.secondAnswer);
        final Button button3 = findViewById(R.id.thirdAnswer);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answers++;
                checkQuestion(v);
                updateQuestion();
                if (curQuestion == null) {
                    GetValue.answers = answers;
                    GetValue.rightAnswers = rightAnswers;
                    try {
                        FileInputStream fIn = openFileInput("stat.txt");
                        String str;
                        byte[] bytes = new byte[fIn.available()];
                        fIn.read(bytes);
                        str = new String(bytes);
                        int totalQuestions = Integer.parseInt(str.replaceAll(" ", "").split("\n")[0]);
                        int totalRightQuestions = Integer.parseInt(str.replaceAll(" ", "").split("\n")[1]);
                        int fullRightTests = Integer.parseInt(str.replaceAll(" ", "").split("\n")[2]);
                        totalQuestions += answers;
                        totalRightQuestions += rightAnswers;
                        fullRightTests += answers == rightAnswers ? 1 : 0;
                        fIn.close();
                        FileOutputStream fOut = openFileOutput("stat.txt", MODE_PRIVATE);
                        OutputStreamWriter fw = new OutputStreamWriter(fOut);
                        fw.write(String.valueOf(totalQuestions) + "\n");
                        fw.write(String.valueOf(totalRightQuestions) + "\n");
                        fw.write(String.valueOf(fullRightTests));
                        fw.close();
                        fOut.close();
                    } catch(Exception ex) {
                        try {
                            FileOutputStream fOut = openFileOutput("stat.txt", MODE_PRIVATE);
                            fOut.write(32);
                            OutputStreamWriter fw = new OutputStreamWriter(fOut);
                            fw.write(String.valueOf(answers) + "\n");
                            fw.write(String.valueOf(rightAnswers) + "\n");
                            fw.write(answers == rightAnswers ? "1" : "0");
                            fw.close();
                            fOut.close();
                        } catch(Exception exc) {
                            System.out.print(exc);
                        }
                    }
                    Intent intent = new Intent(ActivityTasks.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        };
        button1.setOnClickListener(listener);
        button2.setOnClickListener(listener);
        button3.setOnClickListener(listener);
    }

    void updateQuestion() {
        if (engMode) {
            curQuestion = GetValue.getQuestionEnRu(tasks);
        } else {
            curQuestion = GetValue.getQuestionRuEn(tasks);
        }
        if (curQuestion == null) {
            return;
        }
        final Button button1 = findViewById(R.id.firstAnswer);
        final Button button2 = findViewById(R.id.secondAnswer);
        final Button button3 = findViewById(R.id.thirdAnswer);
        if (engMode) {
            button1.setText(curQuestion.answers.get(0).translate);
            button2.setText(curQuestion.answers.get(1).translate);
            button3.setText(curQuestion.answers.get(2).translate);
        } else {
            button1.setText(curQuestion.answers.get(0).word);
            button2.setText(curQuestion.answers.get(1).word);
            button3.setText(curQuestion.answers.get(2).word);
        }
        final TextView textView = findViewById(R.id.questionText);
        textView.setText(curQuestion.getRightAnswer(engMode));
    }

    @Override
    public void onBackPressed() {
        try {
            FileInputStream fIn = openFileInput("stat.txt");
            String str;
            byte[] bytes = new byte[fIn.available()];
            fIn.read(bytes);
            str = new String(bytes);
            int totalQuestions = Integer.parseInt(str.replaceAll(" ", "").split("\n")[0]);
            int totalRightQuestions = Integer.parseInt(str.replaceAll(" ", "").split("\n")[1]);
            int fullRightTests = Integer.parseInt(str.replaceAll(" ", "").split("\n")[2]);
            totalQuestions += answers;
            totalRightQuestions += rightAnswers;
            fullRightTests += answers == rightAnswers && curQuestion == null ? 1 : 0;
            fIn.close();
            FileOutputStream fOut = openFileOutput("stat.txt", MODE_PRIVATE);
            OutputStreamWriter fw = new OutputStreamWriter(fOut);
            fw.write(String.valueOf(totalQuestions) + "\n");
            fw.write(String.valueOf(totalRightQuestions) + "\n");
            fw.write(String.valueOf(fullRightTests));
            fw.close();
            fOut.close();
        } catch(Exception ex) {
            try {
                FileOutputStream fOut = openFileOutput("stat.txt", MODE_PRIVATE);
                fOut.write(32);
                OutputStreamWriter fw = new OutputStreamWriter(fOut);
                fw.write(String.valueOf(answers) + "\n");
                fw.write(String.valueOf(rightAnswers) + "\n");
                fw.write(answers == rightAnswers && curQuestion == null ? "1" : "0");
                fw.close();
                fOut.close();
            } catch(Exception exc) {
                System.out.print(exc);
            }
        }
        finish();
        Intent intent = new Intent(ActivityTasks.this, MainActivity.class);
        startActivity(intent);
        return;
    }

    void checkQuestion(View v) {
        if (v.getId() == R.id.firstAnswer) {
            if (curQuestion.rightAnswer != 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityTasks.this);
                builder.setTitle("Вы ошиблись!")
                        .setMessage("Вы ошиблись! Правильный ответ: " + curQuestion.getRightAnswer(!engMode));
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                rightAnswers++;
            }
        }
        if (v.getId() == R.id.secondAnswer) {
            if (curQuestion.rightAnswer != 1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityTasks.this);
                builder.setTitle("Вы ошиблись!")
                        .setMessage("Вы ошиблись! Правильный ответ: " + curQuestion.getRightAnswer(!engMode));
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                rightAnswers++;
            }
        }
        if (v.getId() == R.id.thirdAnswer) {
            if (curQuestion.rightAnswer != 2) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityTasks.this);
                builder.setTitle("Вы ошиблись!")
                        .setMessage("Вы ошиблись! Правильный ответ: " + curQuestion.getRightAnswer(!engMode));
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                rightAnswers++;
            }
        }
    }
}
