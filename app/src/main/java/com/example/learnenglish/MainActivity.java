package com.example.learnenglish;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import java.io.FileInputStream;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (GetValue.answers != 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Результаты!")
                    .setMessage("Количество вопросов: " + String.valueOf(GetValue.answers) + "\nКоличество правильных ответов: " + String.valueOf(GetValue.rightAnswers));
            AlertDialog alert = builder.create();
            alert.show();
            GetValue.answers = 0;
            GetValue.rightAnswers = 0;
        }
        final Button button1 = findViewById(R.id.enRuButton);
        final Button button2 = findViewById(R.id.ruEnButton);
        final Button button3 = findViewById(R.id.statButton);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityTasks.engMode = true;
                Intent intent = new Intent(MainActivity.this, ActivityTasks.class);
                startActivity(intent);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityTasks.engMode = false;
                Intent intent = new Intent(MainActivity.this, ActivityTasks.class);
                startActivity(intent);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String totalQuestions;
                String totalRightQuestions;
                String fullRightTests;
                try {
                    FileInputStream fIn = openFileInput("stat.txt");
                    String str;
                    byte[] bytes = new byte[fIn.available()];
                    fIn.read(bytes);
                    str = new String(bytes);
                    totalQuestions = str.replaceAll(" ", "").split("\n")[0];
                    totalRightQuestions = str.replaceAll(" ", "").split("\n")[1];
                    fullRightTests = str.replaceAll(" ", "").split("\n")[2];
                } catch (Exception ex) {
                    totalQuestions = "-";
                    totalRightQuestions = "-";
                    fullRightTests = "-";
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Статистика")
                        .setMessage("Количество вопросов: " + totalQuestions + "\nКоличество правильных ответов: " + totalRightQuestions + "\nПолностью правильно решенных тестов: " + fullRightTests);
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
