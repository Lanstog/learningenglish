package com.example.learnenglish;

import java.util.ArrayList;

public class Question {
    ArrayList<Word> answers;
    int rightAnswer;
    Question(ArrayList<Word> answers, int rightAnswer) {
        this.answers = answers;
        this.rightAnswer = rightAnswer;
    }
    String getRightAnswer(boolean en) {
        if (en) {
            return answers.get(rightAnswer).word;
        } else {
            return answers.get(rightAnswer).translate;
        }
    }
}
