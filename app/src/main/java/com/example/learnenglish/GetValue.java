package com.example.learnenglish;

import java.util.ArrayList;
import java.util.Random;

abstract class  GetValue {
    public static int answers = 0;
    public static int rightAnswers = 0;
    public static Question getQuestionEnRu(ArrayList<Word> tasks) {
        if (tasks.size() < 3) {
            return null;
        }
        Random rnd = new Random(System.currentTimeMillis());
        int rndInt = rnd.nextInt(tasks.size());
        Word rightAnswer = tasks.get(rndInt);
        ArrayList<Word> sendTasks = new ArrayList<>();
        sendTasks.add(rightAnswer);
        while (true) {
            Word wrongAnswer = tasks.get(rnd.nextInt(tasks.size()));
            boolean exist = false;
            for (int i = 0; i < sendTasks.size(); i++) {
                if (sendTasks.get(i).translate.equals(wrongAnswer.translate) && sendTasks.get(i).word.equals(wrongAnswer.word)) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                sendTasks.add(wrongAnswer);
            }
            if (sendTasks.size() == 3) {
                break;
            }
        }
        ArrayList<Word> shuffledList = new ArrayList<>();
        while (shuffledList.size() < 3) {
            Word answer = sendTasks.get(rnd.nextInt(sendTasks.size()));
            boolean exist = false;
            for (int i = 0; i < shuffledList.size(); i++) {
                if (answer.translate.equals(shuffledList.get(i).translate) && answer.word.equals(shuffledList.get(i).word)) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                shuffledList.add(answer);
            }
        }
        int rightAnswerNumber = 0;
        for (int i = 0; i < shuffledList.size(); i++) {
            if (rightAnswer.translate.equals(shuffledList.get(i).translate) && rightAnswer.word.equals(shuffledList.get(i).word)) {
                rightAnswerNumber = i;
                break;
            }
        }
        Question question = new Question(shuffledList, rightAnswerNumber);
        tasks.remove(rndInt);
        return question;
    }

    public static Question getQuestionRuEn(ArrayList<Word> tasks) {
        if (tasks.size() < 3) {
            return null;
        }
        Random rnd = new Random(System.currentTimeMillis());
        int rndInt = rnd.nextInt(tasks.size());
        Word rightAnswer = tasks.get(rndInt);
        ArrayList<Word> sendTasks = new ArrayList<>();
        sendTasks.add(rightAnswer);
        while (true) {
            Word wrongAnswer = tasks.get(rnd.nextInt(tasks.size()));
            boolean exist = false;
            for (int i = 0; i < sendTasks.size(); i++) {
                if (sendTasks.get(i).translate.equals(wrongAnswer.translate) && sendTasks.get(i).word.equals(wrongAnswer.word)) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                sendTasks.add(wrongAnswer);
            }
            if (sendTasks.size() == 3) {
                break;
            }
        }
        ArrayList<Word> shuffledList = new ArrayList<>();
        while (shuffledList.size() < 3) {
            Word answer = sendTasks.get(rnd.nextInt(sendTasks.size()));
            boolean exist = false;
            for (int i = 0; i < shuffledList.size(); i++) {
                if (answer.translate.equals(shuffledList.get(i).translate) && answer.word.equals(shuffledList.get(i).word)) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                shuffledList.add(answer);
            }
        }
        int rightAnswerNumber = 0;
        for (int i = 0; i < shuffledList.size(); i++) {
            if (rightAnswer.translate.equals(shuffledList.get(i).translate) && rightAnswer.word.equals(shuffledList.get(i).word)) {
                rightAnswerNumber = i;
                break;
            }
        }
        Question question = new Question(shuffledList, rightAnswerNumber);
        tasks.remove(rndInt);
        return question;
    }

    public static ArrayList<Word> getTasks() {
        ArrayList<Word> tasks = new ArrayList<>();
        tasks.add(new Word("Apple", "Яблоко"));
        tasks.add(new Word("People", "Люди"));
        tasks.add(new Word("Banana", "Банан"));
        tasks.add(new Word("Vova", "Вова"));
        tasks.add(new Word("Nastya", "Настя"));
        tasks.add(new Word("Sun", "Солнце"));
        tasks.add(new Word("White", "Белый"));
        tasks.add(new Word("Job", "Работа"));
        tasks.add(new Word("User", "Пользователь"));
        tasks.add(new Word("Argue", "Спор"));
        tasks.add(new Word("Sport", "Спорт"));
        tasks.add(new Word("Sea", "Море"));
        tasks.add(new Word("School", "Школа"));
        tasks.add(new Word("Table", "Стол"));
        tasks.add(new Word("Desk", "Парта"));

        return tasks;
    }
}
