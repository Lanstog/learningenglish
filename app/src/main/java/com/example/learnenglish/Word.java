package com.example.learnenglish;

public class Word {
    public String word;
    public String translate;
    public Word(String word, String translate) {
        this.word = word;
        this.translate = translate;
    }
}
